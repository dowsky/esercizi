<?php

$arrayassociativo = array(
    'Nome1' => 'Tavolo',
    'Prezzo1' => '50€',
    'Nome2' => 'Sedia',
    'Prezzo2' => '10€',
    'Nome3' => 'Piatto',
    'Prezzo3' => '5€',
    'Nome4' => 'Coltello',
    'Prezzo4' => '2€',
    'Nome5' => 'Cucchiaio',
    'Prezzo5' => '2€',
    'Nome6' => 'Forchetta',
    'Prezzo6' => '3€',
    'Nome7' => 'Padella',
    'Prezzo7' => '5€',
    'Nome8' => 'Fornello',
    'Prezzo8' => '500€',
    'Nome9' => 'Scolapasta',
    'Prezzo9' => '20€',
    'Nome10' => 'Coperchio',
    'Prezzo10' => '10€',
    'Nome11' => 'Bicchiere',
    'Prezzo11' => '3€',
    'Nome12' => 'Tazza',
    'Prezzo12' => '5€',
);


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <style>
        h1{
            font-family: "Raleway", sans-serif;
            font-size: 45px;
            color: cadetblue;
        }
        h3{
            font-family: "Raleway", sans-serif;
            font-size: 25px;
            color: #1094A0;
        }
    </style>
    <title>Categoria</title>
</head>
<body>    
    <h1>Elenco articoli</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="thumbnail text-center">
                    <img src="http://placehold.it/200x200.jpg" alt="">
                    <div class="caption text-center">
                    <h3><?php echo $arrayassociativo['Nome1']; ?></h3>
                    <h3><?php echo $arrayassociativo['Prezzo1']; ?></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="thumbnail text-center">
                    <img src="http://placehold.it/200x200.jpg" alt="">
                    <div class="caption text-center">
                    <h3><?php echo $arrayassociativo['Nome2']; ?></h3>
                    <h3><?php echo $arrayassociativo['Prezzo2']; ?></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="thumbnail text-center">
                    <img src="http://placehold.it/200x200.jpg" alt="">
                    <div class="caption text-center">
                    <h3><?php echo $arrayassociativo['Nome3']; ?></h3>
                    <h3><?php echo $arrayassociativo['Prezzo3']; ?></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="thumbnail text-center">
                    <img src="http://placehold.it/200x200.jpg" alt="">
                    <div class="caption text-center">
                    <h3><?php echo $arrayassociativo['Nome4']; ?></h3>
                    <h3><?php echo $arrayassociativo['Prezzo4']; ?></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="thumbnail text-center">
                    <img src="http://placehold.it/200x200.jpg" alt="">
                    <div class="caption text-center">
                    <h3><?php echo $arrayassociativo['Nome5']; ?></h3>
                    <h3><?php echo $arrayassociativo['Prezzo5']; ?></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="thumbnail text-center">
                    <img src="http://placehold.it/200x200.jpg" alt="">
                    <div class="caption text-center">
                    <h3><?php echo $arrayassociativo['Nome6']; ?></h3>
                    <h3><?php echo $arrayassociativo['Prezzo6']; ?></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="thumbnail text-center">
                    <img src="http://placehold.it/200x200.jpg" alt="">
                    <div class="caption text-center">
                    <h3><?php echo $arrayassociativo['Nome7']; ?></h3>
                    <h3><?php echo $arrayassociativo['Prezzo7']; ?></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="thumbnail text-center">
                    <img src="http://placehold.it/200x200.jpg" alt="">
                    <div class="caption text-center">
                    <h3><?php echo $arrayassociativo['Nome8']; ?></h3>
                    <h3><?php echo $arrayassociativo['Prezzo8']; ?></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="thumbnail text-center">
                    <img src="http://placehold.it/200x200.jpg" alt="">
                    <div class="caption text-center">
                    <h3><?php echo $arrayassociativo['Nome9']; ?></h3>
                    <h3><?php echo $arrayassociativo['Prezzo9']; ?></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="thumbnail text-center">
                    <img src="http://placehold.it/200x200.jpg" alt="">
                    <div class="caption text-center">
                    <h3><?php echo $arrayassociativo['Nome10']; ?></h3>
                    <h3><?php echo $arrayassociativo['Prezzo10']; ?></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="thumbnail text-center">
                    <img src="http://placehold.it/200x200.jpg" alt="">
                    <div class="caption text-center">
                    <h3><?php echo $arrayassociativo['Nome11']; ?></h3>
                    <h3><?php echo $arrayassociativo['Prezzo11']; ?></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="thumbnail text-center">
                    <img src="http://placehold.it/200x200.jpg" alt="">
                    <div class="caption text-center">
                    <h3><?php echo $arrayassociativo['Nome12']; ?></h3>
                    <h3><?php echo $arrayassociativo['Prezzo12']; ?></h3>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</body>
</html>