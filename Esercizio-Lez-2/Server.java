/*
* @author: Vasile Bezer
* Il programma Server riceve dal Client 
* il nome di un un file del direttorio locale
* controlla se il file esiste e conta il numero
* di righe di quel file e manda il risultato al
* Client
*/
import java.io.*;
import java.net.*;

public class Server {

    public static void main(String[] args) throws IOException {
    	// Apro la socket sulla porta 9090
		ServerSocket listener = new ServerSocket(9090);
        try {
	    	System.out.println("Server Running ...");				
			
            while (true) {
				// Accetto le connessioni entranti
                Socket socket = listener.accept();
		        
				// Leggo dalla socket 
				BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		        // Memorizzo nella variabile filename la stringa che arriva sulla socket
				String filename = input.readLine();
				
				/* se la stringa ricevuta non è il nome di un file il programma Server termina*/
			    File f = new File(filename);
				if (!f.exists())
					break;
				
				BufferedReader br = new BufferedReader(new FileReader(f));
				Integer lines = 0;
				while(br.readLine() != null)
					lines++;
				
            	// Scrivo sulla socket il messaggio
			    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                out.println(lines.toString()); 
				
				out.close();
				br.close();
				socket.close();
			}
        }
        finally {
            listener.close();
			System.exit(-1);
        }		
    }
}
