
int main(int argc, char** argv)
{
    if (argc != 3)
    {
        printf("Inserire %s lunghezzaLatoA lunghezzaLatoB\n", argv[0]);
        exit(1);
    }
    printf("L'area del rettangolo è : %d\n", atoi(argv[1]) * atoi(argv[2]));
    exit(0);
}