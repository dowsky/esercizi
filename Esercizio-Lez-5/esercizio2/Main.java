
/**
 * Pixel
 */
public class Main {

    public static void main(String[] args)  {
        Pixel p = new Pixel(21, 23, "rosso");
        System.out.println(p.toString());
    }
}

class Pixel extends Point
{

    private String colore;
    public Pixel(int x, int y, String colore)
    {
        super(x, y);
        this.colore = colore;
    }

    @Override
    public String toString()
    {
        return super.toString() + " " + colore;

    }
}
class Point 
{
/* in java la classe Pixel riesce comunque ad accedere agli attributi private di Point */
    private int x;
    private int y;

    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public String toString()
    {
        return "" + this.x + " " + this.y;
    }
}
