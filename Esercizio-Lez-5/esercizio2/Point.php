<?php

class Point
{
    protected $x;
    protected $y;

    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }
    public function setX(int $x)
    {
        $this->x = $x;
    }
    public function setY(int $y)
    {
        $this->y = $y;
    }
    public function getY()
    {
        return $this->y;
    }
    public function getX() : int
    {
        return $this->x;
    }


    public function toString() : String
    {
        return "Ordinata : " . $this->y . "Ascissa : " . $this->x;
    }
}

?>