
<?php

include 'Point.php';

class Pixel extends Point
{
    /* in php le proprietà private non possono essere accedute dalle classi figlio */
    /* private $x, $y; non funziona */
    private $color;

    public function __construct($x, $y, $color)
    {
        /* La parola chiave parent:: viene usata per accedere a qualsiasi metodo oppure variabile
            della classe che Pixel estende*/
        parent::__construct($x, $y);
        $this->color = $color;
    }
    /* @Override */
    public function toString() : String /* il valore di ritorno deve essere lo stesso della classe parent */
    {
        return "Ascissa: " . $this->x . " " . "Ordinata: " . $this->y . " " .  "Colore: " . $this->color;
    }
}

$ascissa = $_GET['ascissa'];
$ordinata = $_GET['ordinata'];
$colore = $_GET['colore'];


$pixel = new Pixel($ascissa, $ordinata, $colore);

?>


<!DOCTYPE html>

<head>
    <style>
        div
        {
            text-align: center;
            padding: 20%
        }
    </style>
</head>

<html>
    <body>
        <div>
            <?php echo $pixel->toString(); ?> 
        </div>
    </body>
</html>
