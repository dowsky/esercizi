<?php

$rows = array();

$array = array(
    'Nome' => 'Mario',
    'Cognome' => 'Rossi',
    'Salario' => '20000'
);

array_push($rows, $array);
$array = array(
    'Nome' => 'Donald',
    'Cognome' => 'Trump',
    'Salario' => '1'
);

array_push($rows, $array);

/*
$a = [1, 2, 3, 4, 5];

funzionamento di una funzione anonima
function cube($n){
    return $n*$n*$n;
}

print_r( array_map('cube', $a) );

foreach($array as $r){
        $$r['label'] = '<table width="100%">';
        foreach($r['data'] as $s){
            if(isset($s['label']))$$r['label'] .= '<tr><td align="center">'.$s['label'].'</td></tr>'; // The if condition check weather the label is exist or not if yes then add that in particular label's table
        }
        $$r['label'] .= '</table>';
    }
*/
?>


<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <style>
        tr{
            font-family: "Raleway", sans-serif;
            font-size: 25px;
            
        }
        table { 
            border: 3px solid #0D0E08;
            padding: 250px;
            margin-left:auto;
            margin-right:auto;
            margin-top: 10%;
            background-color: #65D1DB;
        }
        thead {
            background-color: #C14128;
        }
        tfoot {
            background-color: #0D0E08; color: #FFF;
         }
         /*colour palette
        *
        * 65D1DB
        * 1094A0
        * F9722E
        * C14128
        * 0D0E08
        *
        */
        </style>
    </head>
    <body>
		<table>
			<thead>
			<tr>
				<th>Nome</th>
				<th>Cognome</th>
				<th>Salario</th>
			</tr>
				</thead>
			<tr>
				<td><?php echo $rows[0]['Nome']; ?></td>
				<td><?php echo $rows[0]['Cognome']; ?></td>
				<td><?php echo $rows[0]['Salario']; ?></td>								
			</tr>
			<tr>
                <td><?php echo $rows[1]['Nome']; ?></td>
				<td><?php echo $rows[1]['Cognome']; ?></td>
				<td><?php echo $rows[1]['Salario']; ?></td>								
			</tr>
            <tfoot>
                <tr>
                    <th>Nome</th>
				    <th>Cognome</th>
				    <th>Salario</th>
                </tr>
            </tfoot>
		</table>
    </body> 
</html>