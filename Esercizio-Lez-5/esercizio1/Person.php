<?php
class Person
{
    private $nome;
    private $cognome;

    public function __construct($nome, $cognome)
    {
        $this->nome = $nome;
        $this->cognome = $cognome;
    }

    public function setNome(String $nome)
    {
        $this->nome = $nome;
    }
    public function setCognome(String $cognome)
    {
        $this->cognome = $cognome;
    }

    public function getNome() : String
    {
        return $this->nome;
    }
    public function getCognome() : String
    {
        return $this->cognome;
    }

    public function stampa()
    {
        echo "Il nome della classe è : " . $this->nome . " " . $this->cognome;
    }

}

$nome = $_GET['nome'];
$cognome = $_GET['cognome'];

$person = new Person($nome, $cognome);

?>

<!DOCTYPE html>
<head>
    <style>
        div
        {
            text-align: center;
            padding: 20%
        }
    </style>
</head>

<html>
    <body>
        <div>
        
            <?php $person->stampa(); ?>
        
        </div>
    </body>
</html>